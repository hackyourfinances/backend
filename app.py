import sys
from flask import Flask, render_template, request, redirect, Response
from flask_cors import CORS
import random, json
import os
from anastasia import *

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/')
def hello_world():
    return "Hello world"

@app.route('/user/<user_id>', methods=['GET'])
def getUsr(user_id):
    print('hello there user: ' + user_id)
    user =  { 
  		'name': "Mary Thompson",
  		'category': "Young Professionsal",
  		'monthly_income': 5000,
  		'goal_name' : 'Pay off student loans',
  		'goal_desc' : 'Pay off 10% of student loans within 6 months.',
  		'goal_target' : 2000,
  		'courses' : [ 
  		{ 
    		'course_name': 'Basic Budgeting',
    		'course_score': 15
  		},
  		{ 
    		'course_name': 'Impact of Interest',
    		'course_score': 74
  		},
  		{	 
    		'course_name': 'Savvy Savings',
    		'cource_score': 11
  		},
  		{ 
    		'course_name': 'Debt Repayment',
    		'cource_score': 32
  		}
  		],
  		'budget': {
	  		'housing': 1250,
	  		'utilities': 250,
	  		'transport': 500,
	  		'food': 500,
	  		'healthcare': 250,
	  		'personal_care': 250,
	  		'entertainment': 250,
	  		'savings': 1000,
	  		'debt_repayment': 500
  		}
	}
    js = json.dumps(user)
    resp = Response(js, status=200, mimetype='application/json')
    return resp
    return user_id
    
@app.route('/goals', methods=['POST'])
def sendGoal():
    goals = request.getjson()
    lst = parsegoals(goalsstr)
    data={}
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp

@app.route('/assets', methods=['PUT'])
def sendAssets():
    # assets = request.getjson()
    # lst = parseassets(assets)
    data={}
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    print('Nice Assets')
    return resp

@app.route('/course', methods=['PUT'])
def getCourse():
    course = request.getjson()
    lst = parsecourse(coursestr)
    data={}
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp

if __name__ == "__main__":
        print(sys.path)
        port = int(os.environ.get("PORT", 5000))
        app.run(host='0.0.0.0', port=port)
