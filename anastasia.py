import sys
import json
goalsstr = '{"user_id":1, "goal_name":"save $", "goal_desc":"save %20 of income this month", "goal_target":200}'
assetsstr = '{"user_id":1, "category":"student", "monthly_income":4000}'
coursestr = '{"user_id":1, "course_id":101, "course_name":"finance lit", "course_score":75}'
#each function takes a JSON string, parses it, and returns key values in a list
def parsegoals(x):
    y = json.loads(x)
    goalslist = []
    goalslist.append(y.get("user_id"))
    goalslist.append(y.get("goal_name"))
    goalslist.append(y.get("goal_desc"))
    goalslist.append(y.get("goal_target"))
    #for item in goalslist:
     #   print(item)
    return goalslist
def parseassets(x):
    y = json.loads(x)
    assetslist = []
    assetslist.append(y.get("user_id"))
    assetslist.append(y.get("category"))
    assetslist.append(y.get("monthly_income"))
    #for item in assetslist:
      #  print(item)
    return assetslist
def parsecourse(x):
    y = json.loads(x)
    courselist = []
    courselist.append(y.get("user_id"))
    courselist.append(y.get("course_id"))
    courselist.append(y.get("course_name"))
    courselist.append(y.get("course_score"))
    #for item in courselist:
        #print(item)
    return parsecourse